//
//  CollectionViewController.h
//  ITherm
//
//  Created by Sandeep Tonapi on 4/26/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CollectionViewController : UICollectionViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    dispatch_queue_t queue;
}

//@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
//@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (nonatomic,strong) NSMutableArray *urlarr;
@property (nonatomic,strong) NSMutableArray *sponsorarr;
@property(nonatomic,retain) NSString *urlstr;
@property(nonatomic,retain) UIActivityIndicatorView  *av;
//@property(nonatomic,retain)NSString *namestr,*unistr,*keynotestr,*abstractstr,*newurlpass,*indxp,*abstr2;
@property(nonatomic,retain)UIImage *keynoteimg;
@property (strong, nonatomic) IBOutlet UICollectionView *cview;

-(void)parsingKeyNotes;
//@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;



@property (nonatomic,strong) NSMutableArray *exhibitionArr,*artArr,*tickArr;
@property(nonatomic,retain)NSString *namestr,*newurlpass,*indxp,*success;

@property (nonatomic,strong) NSMutableArray *namearr,*numberarr,*imgarray,*artidArr,*likedArr;

@property(nonatomic,strong)NSString *imgDescription;
@property(nonatomic,strong)NSString *imgTitle;
@property(nonatomic,strong)NSString *voted_status;
@property(nonatomic,strong)NSString *contestant_id;

@property(nonatomic,strong)NSString *vote_count;
//@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


//@property (weak, nonatomic) IBOutlet UIButton *votingButton;

@end

#import "ScrollViewController.h"
#import "Constant.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface ScrollViewController ()

@end

@implementation ScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _uploadButton.layer.cornerRadius = 8;
    _updateButton.layer.cornerRadius = 5;
    
    [_scrollView setShowsHorizontalScrollIndicator:NO];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    // Do any additional setup after loading the view.
    Attendee *a1=[[Attendee alloc]init];
    
    self.myprofile=[[NSMutableArray alloc]init];
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:getprofile]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(error)
                                          {
                                          }
                                          
                                          NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                          NSError *er=nil;
                                          NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                          if(er)
                                          {
                                          }
                                          
                                          NSArray *myattend=[responseDict objectForKey:@"attendees"];
                                          
                                          for(NSDictionary * dict in myattend)
                                          {
                                              
                                              a1.first_name=dict[@"first_name"];
                                              
                                              a1.first_name = [a1.first_name stringByTrimmingCharactersInSet:
                                                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                              
                                              a1.last_name=[dict objectForKey:@"last_name"];
                                              a1.salutation=[dict objectForKey:@"salutation"];
                                              a1.mobile_no=[dict objectForKey:@"mobile_no"];
                                              a1.country_name=[dict objectForKey:@"country_name"];
                                              a1.role_name=[dict objectForKey:@"role_name"];
                                              a1.emailid=[dict objectForKey:@"emailid"];
                                              a1.role_name=[dict objectForKey:@"role_name"];
                                              a1.affiliation=[dict objectForKey:@"affiliation"];
                                              a1.fax=dict[@"fax"];
                                              a1.userid=dict[@"user_id"];
                                              self.userid=dict[@"user_id"];
                                              
                                              NSString * profile_picUrl = [dict objectForKey:@"profile_pic"];
                                              
                                              NSURL * url = [NSString stringWithFormat:@"%@%@", imagesUrl,profile_picUrl];

                                              _imageName = [NSString stringWithFormat:@"%@%@.jpg",a1.userid,a1.first_name];
                                              
                                              NSLog(@"Name of the image :%@",_imageName);
                                              
                                              dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                  // retrive image on global queue
                                                  UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:url]]];
                                                  
                                                  NSLog(@"The required image is :%@",img);
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      // assign cell image on main thread
                                                      _profilePicture.image = img;
                                                  });
                                              });
                                              
                                              [self.myprofile addObject:a1];
                                              
                                          }
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              if(a1.first_name==(NSString *) [NSNull null])
                                              {
                                                  a1.first_name=@"not given";
                                              }
                                              if(a1.last_name==(NSString *) [NSNull null])
                                              {
                                                  a1.last_name=@"not given";
                                              }
                                              if(a1.salutation==(NSString *) [NSNull null])
                                              {
                                                  a1.salutation=@"";
                                              }
                                              if(a1.role_name==(NSString *) [NSNull null])
                                              {
                                                  a1.role_name=@"not given";
                                              }
                                              if(a1.country_name==(NSString *) [NSNull null])
                                              {
                                                  a1.country_name=@"not given";
                                              }
                                              if(a1.mobile_no==(NSString *) [NSNull null])
                                              {
                                                  a1.mobile_no=@"";
                                              }
                                              if(a1.fax==(NSString *) [NSNull null])
                                              {
                                                  a1.fax=@"";
                                              }
                                              if(a1.affiliation==(NSString *) [NSNull null])
                                              {
                                                  a1.affiliation=@"---";
                                              }
                                              
                                              self.first_name.text=a1.first_name;
                                              self.last_name.text=a1.last_name;
                                              self.country_name.text=a1.country_name;
                                              self.myemail.text=a1.emailid;
                                              self.country_name.text=a1.country_name;
                                              self.affiliation.text=a1.affiliation;
                                              self.mobile_no.text=a1.mobile_no;
                                              self.fax.text=a1.fax;
                                              
                                              
                                              [[NSUserDefaults standardUserDefaults]setValue:a1.salutation forKey:@"salutation"];
                                              //NSLog(@"salutation = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"salutation"]);
                                              
                                              [[NSUserDefaults standardUserDefaults]setValue:a1.first_name forKey:@"firstname"];
                                              //NSLog(@"firstname = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"firstname"]);
                                              [[NSUserDefaults standardUserDefaults]setValue:a1.last_name forKey:@"lastname"];
                                              //NSLog(@"lastname = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"lastname"]);
                                              
                                              
                                          });
                                      }];
    
    [dataTask resume];
    
    //TAP GESTURE TO SELECT IMAGE
    _profilePicture.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    
    tapGesture1.numberOfTapsRequired = 1;
    
    [tapGesture1 setDelegate:self];
    
    [_profilePicture addGestureRecognizer:tapGesture1];
    
}

- (void) tapGesture: (id)sender
{
    
    /*
     UIImagePickerController *picker = [[UIImagePickerController alloc]init];
     picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary, UIImagePickerControllerSourceTypeCamera;
     
     picker.delegate = self;
     picker.allowsEditing = YES;
     [self presentViewController:picker animated:YES completion:nil];
     */
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select Photo from"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    // 1
    /*UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Camera"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              [self takePhoto:firstAction];
                                                              NSLog(@"You pressed button one");
                                                          }]; 
     */
    // 2
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Photo Library"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               NSLog(@"You pressed button two");
                                                               [self selectPhoto:secondAction];
                                                           }]; // 3
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    
    //[alert addAction:firstAction]; // 4
    [alert addAction:secondAction]; // 5
    [alert addAction:ok];
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    //UIButton *button;
    popPresenter.sourceView = _profilePicture;
    popPresenter.sourceRect = _profilePicture.bounds;    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)takePhoto:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)selectPhoto:(UIButton *)sender{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}


- (IBAction)uploadPhoto:(UIButton *)sender {
    //call data uploading function
    [self.indicator startAnimating];
    [self uploadImage];
}

-(void) uploadImage{
    Boolean isParamsSet = false;
    
    NSData *dataImage = UIImageJPEGRepresentation(_profilePicture.image, 1.0f);
    
    //NSLog(@"Data of Image:%@",dataImage);
    
    NSString * urlString = [mainUrl stringByAppendingString:upload_img];
    
    //NSLog(@"URL for images : %@",urlString);
    
    NSMutableURLRequest* request= [[NSMutableURLRequest alloc] init];
    
    if( dataImage != nil){
        
        request.timeoutInterval = 20.0;
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
        [request setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14" forHTTPHeaderField:@"User-Agent"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"pic\"; filename=\"%@\"\r\n", _imageName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        //@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n"
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:dataImage]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        //2
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"tags\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithString:_imageName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        //3
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"emailid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
        [body appendData:[savedValue dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        
        isParamsSet = TRUE;
        
        
    }else{
        
        //response.text = NO_IMAGE;
        
        isParamsSet = FALSE;
    }
    
    if(isParamsSet) {
        NSOperationQueue *queue = [[NSOperationQueue alloc]init];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:queue
                               completionHandler:^(NSURLResponse *urlResponse, NSData *data, NSError *error){
                                   
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       
                                       NSLog(@"Network request completed");
                                       // NSLog(@"Network response: = %@",);
                                       NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                       //NSLog(@"xxxx: = %@",response);
                                       
                                       NSError *error1=nil;
                                       NSDictionary *responsedict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
                                       
                                       UIAlertView* alert;
                                       alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:responsedict[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                       
                                       [_indicator stopAnimating];
                                       
                                       [alert show];
                                       
                                       
                                       [UIApplication sharedApplication].networkActivityIndicatorVisible = FALSE;
                                       
                                       if (error) {
                                           NSLog(@"error:%@", error.localizedDescription);
                                       }
                                   });
                                   
                               }];
    }
    
}

/*
 -(void)dataUploadingInServer1{
 // COnvert Image to NSData
 NSData *dataImage = UIImageJPEGRepresentation(_profilePicture.image, 1.0f);
 
 // set your URL Where to Upload Image
 NSString *urlString = @"http://192.168.1.102/phpmyadmin/itherm/upload_img.php";
 
 
 AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
 [manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
 [formData appendPartWithFileData:dataImage name:@"pic" fileName:[NSString stringWithFormat:@"%@.jpg",_imageName] mimeType:@"image/jpeg"];
 
 [formData appendPartWithFormData:[_imageName dataUsingEncoding:NSUTF8StringEncoding] name:@"tags"];
 
 } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
 NSLog(@"Response: %@",responseObject);
 }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
 NSLog(@"Error: %@",error);
 
 }];
 
 }
 */



//UPLOAD TO SERVER
/*
 -(void)dataUploadingInServer{
 
 //ref https://stackoverflow.com/questions/11084403/uploading-image-via-post-in-objective-c
 // COnvert Image to NSData
 NSData *dataImage = UIImageJPEGRepresentation(_profilePicture.image, 1.0f);
 
 // set your URL Where to Upload Image
 NSString *urlString = @"http://192.168.1.102/phpmyadmin/itherm/upload_img.php";
 
 // set your Image Name
 NSString *filename = _imageName;
 
 // Create 'POST' MutableRequest with Data and Other Image Attachment.
 NSMutableURLRequest* request= [[NSMutableURLRequest alloc] init];
 [request setURL:[NSURL URLWithString:urlString]];
 [request setHTTPMethod:@"POST"];
 NSString *boundary = @"---------------------------14737809831466499882746641449";
 NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
 [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
 NSMutableData *postbody = [NSMutableData data];
 [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
 [postbody appendData:[[NSString stringWithFormat:@"\r\ntags=%@\r\n",_imageName] dataUsingEncoding:NSUTF8StringEncoding]];
 [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"pic\"; filename=\"%@.jpg\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
 [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
 [postbody appendData:[NSData dataWithData:dataImage]];
 [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
 [request setHTTPBody:postbody];
 
 NSLog(@"%@",postbody);
 
 // Get Response of Your Request
 NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
 NSString *responseString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
 NSLog(@"Response  %@",responseString);
 
 
 }*/

//UPLOAD TO SERVER finish



- (IBAction)updateclicked:(id)sender {
    
    NSString *st=[NSString stringWithFormat:@"user_id=%@&first_name=%@&last_name=%@&country_name=%@&emailid=%@&affiliation=%@&mobile_no=%@&fax=%@",self.userid,self.first_name.text,self.last_name.text,self.country_name.text,self.myemail.text,self.affiliation.text,self.mobile_no.text,self.fax.text];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
  
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:update]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[st dataUsingEncoding:NSUTF8StringEncoding]];
    //NSError *error=nil;
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error == nil)
        {
            NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        }
        NSError *er=nil;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
        if(er)
        {
        }
        self.success=[responseDict objectForKey:@"success"];
        self.message=[responseDict objectForKey:@"message"];
        dispatch_async(dispatch_get_main_queue(), ^{
            if([self.success isEqualToString:@"1"])
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success" message:self.message preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                [alertView addAction:ok];
                [self presentViewController:alertView animated:YES completion:nil];
            }
            else
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure" message:@"Profile not updated successfully" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                [alertView addAction:ok];
                [self presentViewController:alertView animated:YES completion:nil];
            }
        });
        
        
    }];
    
    [dataTask resume];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)textFieldDidBeginEditing:(UITextField *)sender
{
    self.activeField = sender;
}

- (IBAction)textFieldDidEndEditing:(UITextField *)sender
{
    self.activeField = nil;
}

- (void) keyboardDidShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:self.activeField.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

-(void)viewWillAppear:(BOOL)animated{
    self.profilePicture.layer.masksToBounds=YES;
    self.profilePicture.layer.borderWidth=1.1;
    self.profilePicture.layer.cornerRadius=self.profilePicture.frame.size.width/2;
    self.profilePicture.layer.borderColor=([UIColor colorWithRed:(211.0/225.0) green:(211.0/225.0) blue:(211.0/255.0)alpha:1.0].CGColor);
    self.profilePicture.clipsToBounds=YES;
    
}

- (IBAction)browseClicked:(UIButton *)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *orginalImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:YES completion:nil];
    self.profilePicture.image = orginalImage;
    
    
}
@end

//
//  publicationTableViewCell.m
//  ITherm
//
//  Created by Sandeep Tonapi on 4/23/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import "PublicationTableViewCell.h"

@implementation PublicationTableViewCell

-(void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end

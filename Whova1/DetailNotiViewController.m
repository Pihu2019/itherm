//
//  DetailNotiViewController.m
//  ITherm
//
//  Created by Anveshak on 4/27/17.
//  Copyright © 2017 Anveshak . All rights reserved.
//

#import "DetailNotiViewController.h"
#import "AbsViewController.h"
#import "UserNotiViewController.h"
@interface DetailNotiViewController ()

@end

@implementation DetailNotiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _name.text=_namestr;
    NSLog(@"%@",_namestr);
    _date.text=_datestr;
    _time.text=_timestr;
    _desctextview.text=_descstr;
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AbsViewController *ab=[segue destinationViewController];
    ab.absStr=_desc.text;
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *mynotification=[defaults objectForKey:@"allnotification"];
    
    NSLog(@"Data ret%@",mynotification);
    
    if(mynotification != NULL){
        for(NSDictionary * dict in mynotification)
        {
            _date.text = dict[@"date"];
            _desc.text = dict[@"description"];
            _name.text = dict[@"name"];
            _time.text = dict[@"time"];
            _desctextview.text = dict[@"description"];
        }
        
    }else{
        //alert no data found
        NSLog(@"NO DATA FOUND");
    }

}

-(void)viewWillDisappear:(BOOL)animated{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"allnotification"];
    [defaults synchronize];
}
-(void)viewDidDisappear:(BOOL)animated{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"allnotification"];
    [defaults synchronize];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  AFNetworkMethods.m
//  ITherm
//
//  Created by Sandeep Tonapi on 4/17/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

-(void)callPostWSWithMethod:(NSString*)methodName withUrl:(NSString*)urlString withParameter:(NSDictionary*)parameter success:(void(^)(id response))successBlock errorBlock:(void(^)(NSError *error))errorBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:urlString parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error;
         id result = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error:&error];
         successBlock(result);
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //NSLog(@"%@",[error description]);
         errorBlock(error);
     }];
}


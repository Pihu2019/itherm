//
//  HotelMapViewController.m
//  ITherm
//
//  Created by Anveshak on 5/23/17.
//  Copyright © 2017 Anveshak . All rights reserved.
//

#import "HotelMapViewController.h"

@interface HotelMapViewController ()

@end

@implementation HotelMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _scrollView.minimumZoomScale = 1.0;
    _scrollView.maximumZoomScale = 5.0;
    _scrollView.contentSize = CGSizeMake(1280, 960);
    _scrollView.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return  _mapImageView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

#import <UIKit/UIKit.h>
@interface PublicationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLable;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
//@property (weak, nonatomic) IBOutlet UITextView *descriptionLable;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLable;

@end

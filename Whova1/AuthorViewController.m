//
//  AuthorViewController.m
//  ITherm
//
//  Created by Anveshak on 2/15/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import "AuthorViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constant.h"
#import "PaperDetails.h"
#import "PublicationTableViewCell.h"
#import "ExchangeContactViewController.h"

@interface AuthorViewController ()

@end

@implementation AuthorViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    [_indicator startAnimating];
    self.authorImage.layer.cornerRadius = self.authorImage.frame.size.width / 2;
    self.authorImage.clipsToBounds = YES;
    self.authorImage.layer.borderColor=[UIColor blackColor].CGColor;
    
    
    _authorName.text = _authorNameStr;
    _authorUniversity.text = _universityStr;
    
    _papername.text=_papernameStr;
    _paperid.text=_paperidstr;
    _starttime.text=_starttimeStr;
    _endtime.text=_endtimeStr;
    _authorid.text=_authoridStr;
    
    _tableView1.delegate = self;
    _tableView1.dataSource = self;
    
    _connect.layer.cornerRadius = 10;
    
   // [self authorDataParsing];
    [self authorPaperParsing];
}

-(void)viewWillAppear:(BOOL)animated
{
 
    int i=[_indxpath intValue];
    NSString *tempimgstr=[_kimgarray objectAtIndex:i];
    
    [_authorImage sd_setImageWithURL:[NSURL URLWithString:tempimgstr]
                    placeholderImage:[UIImage imageNamed:@"default.png"]];
        
    _paperInfoArray = [[NSMutableArray alloc]init];
    
}

-(void)authorPaperParsing{
    
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@&author_id=%@",emailid,_authoridStr];
    //NSLog(@"paper_id...& AUTHOR ID....%@",myst);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_author_papers]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURLSessionDataTask *task=[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        if(data==nil)
                                        {
                                            NSLog(@"Data is nil");
                                        }
                                        else
                                        {
                                            //NSLog(@"Rhuiiii%@",response);
                                            NSDictionary *outerdic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                            
                                           // NSLog(@"xxx :%@", outerdic);
                                            _paperArray = [outerdic objectForKey:@"full_papers"];
                                            
                                            //NSLog(@"THis is key arrray%@",_paperArray);
                                            for(NSDictionary *temp in _paperArray){
                                                
                                                //AuthorDetails *f=[[AuthorDetails alloc]init];
                                                PaperDetails *ppr = [[PaperDetails alloc]init];
                                                ppr.start_time = [temp objectForKey:@"start_time"];
                                                ppr.date = [temp objectForKey:@"date"];
                                                ppr.room_name = [temp objectForKey:@"room_name"];
                                                ppr.paper_name = [temp objectForKey:@"paper_name"];
                                              
                                                //NSLog(@"Start Time:%@",ppr.start_time);
                                                
                                                [_paperInfoArray addObject:ppr];
                                              
                                                [_tableView1 reloadData];
                                           
                                            }
                                            [_tableView1 reloadData];
                                           // NSLog(@"COunt:%lu",(unsigned long)_paperInfoArray.count);
                                        }
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [_tableView1 reloadData];
                                        });
                                    });
                                }];
    [task resume];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _paperInfoArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PublicationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    [_indicator stopAnimating];
    
    PaperDetails *ppr = _paperInfoArray[indexPath.row];
    cell.descriptionLable.text = ppr.paper_name;
    cell.timeLable.text = ppr.start_time;
    cell.dateLabel.text = ppr.date;
    cell.locationLabel.text = ppr.room_name;
    return cell;
}


 /*-(void)authorDataParsing{
    NSString *myst=[NSString stringWithFormat:@"paper_id=%@&author_id=%@",_paperidstr,_authoridStr];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:author_details]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *task=[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                {
                                    
                                    if(data==nil)
                                    {
                                        NSLog(@"Data is nil");
                                    }
                                    else
                                    {
                                        //NSLog(@"%@",response);
                                        NSDictionary *outerdic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                        
                                        
                                        NSArray *keyarr=[outerdic objectForKey:@"authorinfo"];
                                        
                                        for(NSDictionary *temp in keyarr)
                                        {
                                            NSString *str1=[temp objectForKey:@"salutation"];
                                            NSString *str2=[temp objectForKey:@"first_name"];
                                            NSString *str3=[temp objectForKey:@"last_name"];
                                            NSString *str4=[temp objectForKey:@"images"];
                                            NSString *str5=[temp objectForKey:@"affiliation"];
                                            NSString *str6=[temp objectForKey:@"designation"];
                                            NSString *str7=[temp objectForKey:@"university"];
                                            NSString *str8=[temp objectForKey:@"biography"];
                                            NSString *str9=[temp objectForKey:@"paper_id"];
                                            
                                            if([str9 isEqualToString:_paperidstr])
                                            {
                                                _salutationStr=str1;
                                                _firstnameStr=str2;
                                                _lastnameStr=str3;
                                                _affiliationStr=str5;
                                                _designationStr=str6;
                                                _universityStr=str7;
                                                
                                                
                                                if([_biographyStr isEqual:[NSNull null]])
                                                {
                                                    _biographyStr=@"--";
                                                }
                                                else
                                                {
                                                    _biographyStr=str8;
                                                }
                                                NSString *tempimgstr=str4;
                                               
                                                [_authorImage sd_setImageWithURL:[NSURL URLWithString:tempimgstr]
                                                                placeholderImage:[UIImage imageNamed:@"default.png"]];
                                                
                                                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                                    
                                                    _authorName.text=[str1 stringByAppendingFormat:@"%@ %@",str2,str3];
                                                    
                                                    _authorNameExchange.text=[@"Keep in touch with " stringByAppendingFormat:@"%@ %@ %@  %s",str1,str2,str3," by exchanging contact info"];
                                                    
                                                    if([_universityStr isEqual:[NSNull null]])
                                                    {
                                                        _authorUniversity.text=@"--";
                                                    }
                                                    else
                                                    {
                                                        _authorUniversity.text=_affiliationStr;
                                                    }
                                                    
                                                    _authorAffiliation.text=_affiliationStr;
                                                    _authorDesignation.text=_designationStr;
                                                
                                                }];
                                            }
                                        }
                                        
                                    }
                                    
                                }];
    [task resume];
}*/

-(IBAction)exchangeBtnClicked:(id)sender {
    //[self performSegueWithIdentifier:@"exchange" sender:NULL];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"exchange"]){
        ExchangeContactViewController *destViewController = segue.destinationViewController;
        
        destViewController.authorID = _authoridStr;
    }
}

@end

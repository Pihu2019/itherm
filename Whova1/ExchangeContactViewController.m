//
//  ExchangeContactViewController.m
//  ITherm
//
//  Created by Anveshak on 2/15/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import "ExchangeContactViewController.h"
#import "Constant.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AuthorViewController.h"

@interface ExchangeContactViewController ()

@end

@implementation ExchangeContactViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    _saveBtn.layer.cornerRadius = 10;
}

-(void)viewWillAppear:(BOOL)animated{
    //NSLog(@"Author ID :%@",_authorID);
    [self getNameParsing];

}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getNameParsing{
    
    //Attendee *a1=[[Attendee alloc]init];
    
    //self.myprofile=[[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:getprofile]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error=nil;
    
    if(error)
    {
        NSLog(@"Error encountered");
    }
    
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(data != nil){
                                              if(error)
                                              {
                                                  NSLog(@"Error encountered");
                                              }
                                              NSError *er=nil;
                                              NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                              if(er)
                                              {
                                              }
                                              
                                              NSArray *myattend=[responseDict objectForKey:@"attendees"];
                                              
                                              //NSLog(@"Data receiver : %@",responseDict);
                                              
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  for(NSDictionary * dict in myattend)
                                                  {
                                                      _firstName.text = [dict objectForKey:@"first_name"];
                                                      _lastName.text = [dict objectForKey:@"last_name"];
                                                      _country.text = [dict objectForKey:@"country_name"];
                                                      _email.text = [dict objectForKey:@"emailid"];
                                                      _affiliation.text = [dict objectForKey:@"affiliation"];
                                                      
                                                  }
                                                  
                                              });
                                          }
                                      }];
    
    [dataTask resume];
    
}

- (IBAction)savedBtn:(UIButton *)sender {
    [self exchangeInfo];
}

-(void)exchangeInfo
{
    [_indicator startAnimating];
    
    NSString *myst=[NSString stringWithFormat:@"first_name=%@&last_name=%@&country=%@&emailid=%@&affiliation=%@&mobile=%@&fax=%@&skype_id=%@&&we_chat_id=%@&author_id=%@",_firstName.text,_lastName.text,_country.text,_email.text,_affiliation.text,_mobileNo.text,_fax.text,_skypeId.text,_weChatId.text,_authorID];
    
    NSLog(@"My string :%@",myst);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:contact_author_local]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error=nil;
    
    NSLog(@"error=%@",error);
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error == nil)
        {
            NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
            NSLog(@"data =%@",text);
            
        }
        NSError *er=nil;
        
        NSDictionary *dictionary=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
        
        NSLog(@"Response Dict :%@",dictionary);
        
        if(er)
        {
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_indicator stopAnimating];
         
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:[dictionary objectForKey:@"message"] delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
        });
    }];
    //[self dismissViewControllerAnimated:NO completion:nil];
    [dataTask resume];
}

//DISMISS CURRENT VIEW ON OK OF ALERT
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:
(NSInteger)buttonIndex{

    //NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    
   /*
    AuthorViewController *AC = nil;
    
    if ([buttonTitle isEqual:@"ok"]) // Check for Ok button
    {
    //    [self dismissViewControllerAnimated:YES completion:nil];
        if (AC) {
            //[self.navigationController popToViewController:AC animated:YES];
        }
    }
    */
}

@end

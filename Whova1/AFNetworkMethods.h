//
//  AFNetworkMethods.h
//  ITherm
//
//  Created by Sandeep Tonapi on 4/17/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#ifndef AFNetworkMethods_h
#define AFNetworkMethods_h
typedef void(^downloadBlock)(id success,NSError *error);

+(void)callPostWSWithMethod:(NSString*)methodName withUrl:(NSString*)urlString withParameter:(NSDictionary*)parameter success:(void(^)(id response))successBlock errorBlock:(void(^)(NSError *error))errorBlock;

+(void)callGetWSWithMethod:(NSString*)methodName withUrl:(NSString*)urlString withParameter:(NSDictionary*)parameter Block:(downloadBlock)block;

+(void)callPutWSWithMethod:(NSString*)methodName withUrl:(NSString*)urlString withParameter:(NSDictionary*)parameter success:(void(^)(id response))successBlock errorBlock:(void(^)(NSError *error))errorBlock;




#endif /* AFNetworkMethods_h */

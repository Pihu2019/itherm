//
//  MainViewController.m
//  ITherm
//
//  Created by Anveshak on 2/27/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import "MarqueeLabel.h"
#import "MainViewController.h"
#import "Constant.h"
#import "UpcomingSession.h"
#import "UpcomingSessionTableViewCell.h"
#import "CourseViewController.h"
#import "SessionViewController.h"
#import "SessTalkViewController.h"
#import "InvitedInfoViewController.h"
#import "BreakViewController.h"
#import "KeyViewController.h"
#import "PosterViewController.h"
#import "TechTalkNewViewController.h"
#import "WomenViewController.h"
#import "NotificationsViewController.h"
#import "UserNotiViewController.h"
#import "DetailNotiViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(handleTap:)];
    gesRecognizer.delegate = self;
    
    [_tapNotificationView addGestureRecognizer:gesRecognizer];
    
    //view updates
    _latestNotification=[[NSMutableArray alloc]init];
    
    _que=[[NSOperationQueue alloc]init];
    
    self.view1.layer.masksToBounds=YES;
    self.view1.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.view1.layer.cornerRadius=5;
    self.view1.layer.borderWidth=0.2;
    
    self.view2.layer.masksToBounds=YES;
    self.view2.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.view2.layer.cornerRadius=5;
    self.view2.layer.borderWidth=0.2;
    
    self.view3.layer.masksToBounds=YES;
    self.view3.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.view3.layer.cornerRadius=5;
    self.view3.layer.borderWidth=0.2;
    
    self.profilePic.layer.masksToBounds=YES;
    self.profilePic.layer.borderColor=_profilePic.layer.borderColor=([UIColor colorWithRed:(211.0/225.0) green:(211.0/225.0) blue:(211.0/255.0)alpha:1.0].CGColor);
    
    self.profilePic.layer.borderWidth=1.1;
    self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2;
    self.profilePic.clipsToBounds = YES;
    
    
    self.signInBtn.layer.masksToBounds=YES;
    self.signInBtn.layer.cornerRadius=5;
    
    self.registerBtn.layer.masksToBounds=YES;
    self.registerBtn.layer.cornerRadius=5;
    
    self.viewSessionBtn.layer.masksToBounds=YES;
    self.viewSessionBtn.layer.cornerRadius=5;
    
    self.addSchBtn.layer.masksToBounds=YES;
    self.addSchBtn.layer.cornerRadius=5;
    
    
    self.btnTotalSession.layer.masksToBounds=YES;
    self.btnTotalSession.layer.cornerRadius=5;
    self.btnTotalSession.layer.borderColor=([UIColor colorWithRed:(84.0/225.0) green:(211.0/225.0) blue:(199.0/255.0)alpha:1.0].CGColor);
    self.btnTotalSession.layer.borderWidth=1.0;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _btnTotalSession.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:(208.0/225.0) green:(234.0/225.0) blue:(229.0/255.0)alpha:1.0].CGColor, (id)[UIColor colorWithRed:(131.0/225.0) green:(234.0/225.0) blue:(229.0/255.0)alpha:1.0].CGColor];
    
    //[_btnTotalSession.layer insertSublayer:gradient atIndex:0];
    
    
    self.keynoteBtn.layer.masksToBounds=YES;
    self.keynoteBtn.layer.cornerRadius=8;
    self.keynoteBtn.layer.borderColor=([UIColor colorWithRed:(88.0/225.0) green:(211.0/225.0) blue:(244.0/255.0)alpha:1.0].CGColor);
    self.keynoteBtn.layer.borderWidth=1.0;
    
    CAGradientLayer *gradient1 = [CAGradientLayer layer];
    
    gradient1.frame = _keynoteBtn.bounds;
    gradient1.colors = @[(id)[UIColor colorWithRed:(178.0/225.0) green:(239.0/225.0) blue:(239/255.0)alpha:1].CGColor, (id)[UIColor colorWithRed:(138/225.0) green:(239/225.0) blue:(239/255.0)alpha:1].CGColor];
    
    //[_keynoteBtn.layer insertSublayer:gradient1 atIndex:0];
    
    
    self.myScheduleBtn.layer.masksToBounds=YES;
    self.myScheduleBtn.layer.cornerRadius=8;
    self.myScheduleBtn.layer.borderColor=([UIColor colorWithRed:(247.0/225.0) green:(215.0/225.0) blue:(79.0/255.0)alpha:1.0].CGColor);
    self.myScheduleBtn.layer.borderWidth=1.0;
    
    
    CAGradientLayer *gradient2 = [CAGradientLayer layer];
    
    gradient2.frame = _myScheduleBtn.bounds;
    gradient2.colors = @[(id)[UIColor colorWithRed:(242/225.0) green:(226/225.0) blue:(174/255.0)alpha:1].CGColor, (id)[UIColor colorWithRed:(242/225.0) green:(209/225.0) blue:(116/255.0)alpha:1].CGColor];
    
    //[_myScheduleBtn.layer insertSublayer:gradient2 atIndex:0];
    
    self.navigationItem.hidesBackButton = NO;
    [self setupScrollView2:self.scroll2];
    
    
    UIPageControl *pgCtr2=[[UIPageControl alloc]init];//WithFrame:CGRectMake(0, 264, 480,50)];
    [pgCtr2 setTag:5];
    pgCtr2.numberOfPages=11;
    pgCtr2.autoresizingMask=UIViewAutoresizingNone;
    [self.contentview2 addSubview:pgCtr2];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaLTStd-Roman" size:10.0f], NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaLTStd-Roman" size:10.0f], NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateHighlighted];
    
    //    [_viewSessionBtn addTarget:self action:@selector(buttonPressed:)
    //     forControlEvents:UIControlEventTouchUpInside];
    
    _tableview2.delegate=self;
    _tableview2.dataSource=self;
    
    _upcomingSessions=[[NSMutableArray alloc]init];
    _upcomingDateArray=[[NSMutableArray alloc]init];
    _upcomingTimeArray=[[NSMutableArray alloc]init];
    _upcomingSessionArr=[[NSMutableArray alloc]init];
    _upcomingLocationArr=[[NSMutableArray alloc]init];
    _upcomingSessDetailArr=[[NSMutableArray alloc]init];
    
    //[self getScheduledStatus];
    [self checkIfAttendee];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOTE!"
                                                    message:@"To Vote in Art in Science Competition navigate to More -> Art in Science."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

-(void)checkIfAttendee{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];

    NSURL * url = [NSURL URLWithString:attendeeCheck];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error == nil)
            {
                NSError *er=nil;
                
                NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
//                NSLog(@"R ** :%@",responseDict);
//                NSLog(@"R ** :%@",responseDict[@"valid"]);
                if(responseDict[@"valid"] != nil){
                    NSString *validAttendee = responseDict[@"valid"];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:validAttendee forKey:@"valid_status"];
                    [defaults synchronize];
                }
            }
         });
    }];
    [dataTask resume];
    
    
}

- (IBAction)keyNoteButton:(UIButton *)sender {
    
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    if (emailid==NULL){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainViewController *detailViewController = [storyboard instantiateViewControllerWithIdentifier:@"KeynoteViewController"];
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    }else{
        //NSLog(@"Do Nothing");
    }
    
}

// Declare the Gesture Recogniser handler method.
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer{
    UserNotiViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailNotiViewController"];
    [self.navigationController pushViewController:user animated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated{
    _notificationView.layer.cornerRadius = 5;
    _notification.layer.masksToBounds = true;
    
    [self upcomingSessionParsing];
    [self.navigationItem setHidesBackButton:YES];
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    
    if (emailid==NULL){
        _view2.hidden = NO;
        _afterLoginView.hidden = YES;
        
        [self getKeynoteCount];
        [self getTotalSessionCount];
        
    }else{
        _afterLoginView.hidden = NO;
        _view2.hidden = YES;
        _sessionsAttendedArrow.hidden = YES;
        //_keynoteCount.userInteractionEnabled = NO;
        
        
        [self dataFetchingLatest];
        [self getTodaySessionCount];
        [self getSessionAttendedCount];
        [self getSheduledItemCount];
        
        [self getRoleID];
        
        //navbar icon
        UIBarButtonItem *Savebtn=[[UIBarButtonItem alloc]initWithImage:
                                  [[UIImage imageNamed:@"bell_ipad.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                                 style:UIBarButtonItemStylePlain target:self action:@selector(extracted)];
        self.navigationItem.rightBarButtonItem=Savebtn;
    }
}

-(void)getRoleID{
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"user=%@",savedValue];
    
    
    //NSLog(@"get data email ID.....:%@",myst);
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:two_2]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error == nil)
            {
                NSError *er=nil;
                
                NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                
                NSLog(@"R ** :%@",responseDict);
                if(responseDict[@"status"] != nil){
                    NSString *role_id = responseDict[@"status"];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:role_id forKey:@"role_id"];
                    [defaults synchronize];
                }
                
                //NSLog(@"Role ID%@",role_id);
                
                //NSLog(@"Data :%@",[[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding]);
               
                
                //NSLog(@"Data :%@",responseDict[@"status"]);
            }
            
        });
        
        
    }];
    
    [dataTask resume];
    
}

-(void)extracted{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *role_id = [defaults objectForKey:@"role_id"];
    NSLog(@"NSArray :%@",role_id);
    
    if([role_id  isEqual: @"13"]){
        NotificationsViewController *admin = [self.storyboard instantiateViewControllerWithIdentifier:@"adminController"];
        [self.navigationController pushViewController:admin animated:YES];
        
    }else{
        UserNotiViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"userController"];
        [self.navigationController pushViewController:user animated:YES];
    }
    
}

-(void)dataFetchingLatest{
    //fetch the view data heres//
    [_latestNotification removeAllObjects];
    
    NSString *savedvalue=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedvalue];//2
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_latest_notification]];
    
    //NSLog(@"URL %@",url);
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if (error) {
        //NSLog(@"%@",error.description);
    }
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //dispatch_async(dispatch_get_main_queue(), ^
            if(error == nil)
            {
                
                NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                
                NSError *er=nil;
                NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                
                //NSLog(@"Response%@",responseDict);
                if(er)
                {
                    
                }
                
                //Error
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSArray *mynotification=[responseDict objectForKey:@"all_notifications"];
                    
                    NSArray *matches = [mynotification valueForKey: @"id"];
                    
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:mynotification forKey:@"allnotification"];
                    [defaults synchronize];
                    
                    if(mynotification.count==0)
                    {
                        //                    CGRect labelFrame = CGRectMake(55,160,320,130);
                        //
                        //                    UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
                        //
                        //                    myLabel.backgroundColor = [UIColor clearColor];
                        //                    myLabel.textColor = [UIColor lightGrayColor];
                        //                    myLabel.font = [UIFont fontWithName:@"Verdana" size:15.0];
                        //                    //myLabel.textAlignment=NSTextAlignmentCenter;
                        //                    myLabel.numberOfLines = 2;
                        //                    myLabel.text = @"Currently no new Notifications.";
                        //
                        //                    myLabel.shadowColor = [UIColor darkGrayColor];
                        //                    myLabel.shadowOffset = CGSizeMake(1.0,1.0);
                        //
                        //                    [self.view addSubview:myLabel];
                        
                    }else{
                        
                        for(NSDictionary * dict in mynotification)
                        {
                            
                            //NSLog(@"we have%@",mynotification);
                            MainViewController *obj=[[MainViewController alloc]init];
                            _notification.text = dict[@"name"];
                            _date.text = dict[@"date"];
                            _desc.text = dict[@"description"];
                        }
                        
                    }
                    
                });
            }
            
        });
        
        
    }];
    [dataTask resume];
    
}

-(void)getTodaySessionCount{
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];
    
    
    // NSLog(@"get data email ID.....:%@",myst);
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_today_session_count]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error == nil)
            {
                
                NSString *text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                
                _totalSessionCount.text=text;
                _sessionsTodayLabel.text = @"Sessions today";
                
            }
        });
        
        
    }];
    
    [dataTask resume];
    
}

-(void)getSessionAttendedCount{
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];
    
    
    //NSLog(@"get data email ID.....:%@",myst);
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_session_attended_count]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error == nil)
            {
                
                NSString *text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                
                _keynoteCount.text=text;
                _sessionsAttendedLabel.text = @"Sessions attended";
            }
        });
        
        
        
    }];
    
    [dataTask resume];
    
}

-(void)getSheduledItemCount{
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];
    
    
    //NSLog(@"get data email ID.....:%@",myst);
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_scheduled_item_count]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error == nil)
            {
                
                NSString *text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                
                _itemsInSchedule.text=text;
                // NSLog(@"scheduled item count==%@",_scheduledItemsCount.text);
                
            }
            
        });
        
        
    }];
    
    [dataTask resume];
    
}


-(void)upcomingSessionParsing{
    
    [_upcomingDateArray removeAllObjects];
    [_upcomingTimeArray removeAllObjects];
    [_upcomingSessionArr removeAllObjects];
    [_upcomingLocationArr removeAllObjects];
    [_upcomingSessDetailArr removeAllObjects];
    [_upcomingSessions removeAllObjects];
    
    NSURL * urlstr = [NSURL URLWithString:[mainUrl stringByAppendingString:get_upcoming_session]];
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:urlstr];
    
    NSString *savedValue=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionConfiguration *configuration=[NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session=[NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                {
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        if(data==nil)
                                        {
                                            //NSLog(@"Data is nil");
                                        }
                                        
                                        else
                                        {
                                            NSDictionary *outerdic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                            
                                            NSArray *keyarr=[outerdic objectForKey:@"upcoming_sessions"];
                                            
                                            NSLog(@"Key Array : ***  %@",keyarr);
                                            
                                            for(NSDictionary *temp in keyarr)
                                            {
                                                NSString *str1=[temp objectForKey:@"session_id"];
                                                NSString *str2=[temp objectForKey:@"session_name"];
                                                NSString *str3=[temp objectForKey:@"start_time"];
                                                NSString *str4=[temp objectForKey:@"date"];
                                                NSString *str5=[temp objectForKey:@"room_name"];
                                                NSString *str6=[temp objectForKey:@"program_type_id"];
                                                NSString *str7=[temp objectForKey:@"category"];
                                                NSString *str8=[temp objectForKey:@"session_details"];
                                                NSString *str9=[temp objectForKey:@"end_time"];
                                                
                                                NSString *str10=[temp objectForKey:@"status"];
                                                
                                                //NSLog(@"str2  %@",str2);
                                                UpcomingSession *k1=[[UpcomingSession alloc]init];
                                                k1.sessionidStr=str1;
                                                k1.sessionNameStr=str2;
                                                k1.startTimeStr=str3;
                                                k1.dateStr=str4;
                                                k1.roomnameStr=str5;
                                                k1.programidStr=str6;
                                                k1.categoryStr=str7;
                                                k1.sessionDetailsStr=str8;
                                                k1.endtimeStr=str9;
                                                k1.status = str10;
                                                
                                                
                                                [[NSUserDefaults standardUserDefaults]setValue:str1 forKey:@"sesionid"];
                                                [[NSUserDefaults standardUserDefaults]setValue:str6 forKey:@"programtypeid"];
                                                //NSLog(@"***sesion ID = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"sesionid"]);
                                                
                                                [_upcomingSessions addObject:k1];
                                                
                                                //[_tableview2 reloadData];
                                            }
                                            //[_tableview2 reloadData];
                                        }
                                        [_tableview2 performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
                                        
                                    
                                        //[self getScheduledStatus];
                                    });
                                    
                                }];
    [task resume];
    
    [_tableview2 reloadData];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.upcomingSessions.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if (tableView==self.tableview2) {
        
        UpcomingSessionTableViewCell *cell = [self.tableview2 dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        UpcomingSession *ktemp=[_upcomingSessions objectAtIndex:indexPath.row];
        cell.startTime.text=ktemp.startTimeStr;
        cell.upcomingDate.text=ktemp.dateStr;
        cell.upcomingLocation.text=ktemp.roomnameStr;
        cell.upcomingSessionName.text=ktemp.sessionNameStr;
        cell.upcomingSessionDetails.text=ktemp.sessionDetailsStr;
        cell.sessionid.text=ktemp.sessionidStr;
        cell.endtime.text=ktemp.endtimeStr;
        cell.category.text=ktemp.categoryStr;
        [cell.addMySchBtn addTarget:self action:@selector(buttonclicked:) forControlEvents:UIControlEventTouchUpInside];

        NSLog(@"Value :%@",ktemp.status);
        
        cell.addMySchBtn.enabled = FALSE;
        if([ktemp.status isEqualToString:@"1"]){
            
            [cell.addMySchBtn setTitle:@"Added to my schedule" forState:UIControlStateNormal];
            // cell.sessLabel.text = @"Added to my schedule";
        }else{
            [cell.addMySchBtn setTitle:@"Add to my schedule" forState:UIControlStateNormal];
            //cell.sessLabel.text = @"Add to my schedule";
        }
        cell.addMySchBtn.enabled = TRUE;
    }
    else{
        // NSLog(@"Do Nothing");
    }
    return cell;
    
}

-(void)setupScrollView2:(UIScrollView *)scrMain{
    for (int i=1; i<=11; i++) {
        UIImage *image1=[UIImage imageNamed:[NSString stringWithFormat:@"Ibm.png"]];
        UIImage *image2=[UIImage imageNamed:[NSString stringWithFormat:@"cave3.png"]];
        UIImage *image3=[UIImage imageNamed:[NSString stringWithFormat:@"dupont.jpg"]];
        UIImage *image4=[UIImage imageNamed:[NSString stringWithFormat:@"intel.jpg"]];
        UIImage *image5=[UIImage imageNamed:[NSString stringWithFormat:@"laird.png"]];
        UIImage *image6=[UIImage imageNamed:[NSString stringWithFormat:@"maryland.png"]];
        UIImage *image7=[UIImage imageNamed:[NSString stringWithFormat:@"Novark.png"]];
        UIImage *image8=[UIImage imageNamed:[NSString stringWithFormat:@"nsf.png"]];
        UIImage *image9=[UIImage imageNamed:[NSString stringWithFormat:@"huawei.png"]];
        UIImage *image10=[UIImage imageNamed:[NSString stringWithFormat:@"S3ip.png"]];
        
        [_imgView2 setImage:image1];
     _imgView2.animationImages=@[image1,image2,image3,image4,image5,image6,image7,image8,image9];
        _imgView2.animationDuration=25.0;
        _imgView2.tag=i+1;
        
        [_imgView2 startAnimating];
    }
    
    
    [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(scrollingTimer2) userInfo:nil repeats:YES];
}
-(void)scrollingTimer2{
    //UIScrollView *scrMain=(UIScrollView *)[self.view viewWithTag:1];
    UIPageControl *pgCtr2=(UIPageControl*)[self.view viewWithTag:24];
    CGFloat contentOffset=self.scroll2.contentOffset.x;
    
    
    int nextPage=(int)(contentOffset/self.scroll2.frame.size.width)+1;
    
    if (nextPage!=12){
        [self.scroll2 scrollRectToVisible:CGRectMake(nextPage*self.scroll2.frame.size.width,0,self.scroll2.frame.size.width,self.scroll2.frame.size.height)animated:YES];
        pgCtr2.currentPage=nextPage;
    }else{
        [self.scroll2 scrollRectToVisible:CGRectMake(0,0,self.scroll2.frame.size.width,self.scroll2.frame.size.height)animated:YES];
        
        pgCtr2.currentPage=0;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getKeynoteCount{
    
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_keynote_count]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionConfiguration *configuration=[NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session=[NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        if(error == nil)
                                        {
                                            NSDictionary *outerdic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                            
                                            
                                            NSString *text=[outerdic objectForKey:@"total_keynotes"];
                                            _keynoteCount.text=text;
                                            //NSLog(@"keynoteCount count==%@",_keynoteCount.text);
                                            
                                        }
                                    });
                                    
                                }];
    
    [task resume];
}
-(void)getTotalSessionCount{
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_total_session_count]];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionConfiguration *configuration=[NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session=[NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                {
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        if(error == nil)
                                        {
                                            NSDictionary *outerdic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                            
                                            NSString *text=[outerdic objectForKey:@"total_sessions"];
                                            _totalSessionCount.text=text;
                                            //NSLog(@"totalSessionCount count==%@",_totalSessionCount.text);
                                            
                                        }
                                    });
                                    
                                }];
    
    [task resume];
    
}



/*-(void)upcomingSessionparsing
 {
 
 NSBlockOperation *op1=[NSBlockOperation blockOperationWithBlock:^{
 
 NSURL * urlstr = [NSURL URLWithString:[mainUrl stringByAppendingString:get_upcoming_session]];
 
 NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:urlstr];
 
 NSURLSessionConfiguration *configuration=[NSURLSessionConfiguration defaultSessionConfiguration];
 
 NSURLSession *session=[NSURLSession sessionWithConfiguration:configuration];
 
 NSURLSessionDataTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
 {
 
 if(data==nil)
 {
 NSLog(@"Data is nil");
 
 }
 
 else
 {
 
 
 NSLog(@"upcoming sessionn===%@",response);
 
 
 NSDictionary *outerdic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
 
 
 NSArray *keyarr=[outerdic objectForKey:@"upcoming_sessions"];
 
 for(NSDictionary *temp in keyarr)
 {
 NSString *str1=[temp objectForKey:@"session_id"];
 NSString *str2=[temp objectForKey:@"session_name"];
 NSString *str3=[temp objectForKey:@"start_time"];
 NSString *str4=[temp objectForKey:@"date"];
 NSString *str5=[temp objectForKey:@"room_name"];
 NSString *str6=[temp objectForKey:@"program_type_id"];
 NSString *str7=[temp objectForKey:@"category"];
 NSString *str8=[temp objectForKey:@"session_details"];
 
 _sessionidStr=str1;
 _sessionNameStr=str2;
 _startTimeStr=str3;
 _dateStr=str4;
 _roomnameStr=str5;
 _programidStr=str6;
 _categoryStr=str7;
 _sessionDetailsStr=str8;
 
 _sessionName.text=_sessionNameStr;
 _startTime.text=_startTimeStr;
 _CurrentDate.text=_dateStr;
 _location.text=_roomnameStr;
 _sessionDetails.text=_sessionDetailsStr;
 
 }
 
 }
 
 }];
 [task resume];
 
 }];
 
 [_que addOperation:op1];
 
 }
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UpcomingSessionTableViewCell *cell = [self.tableview2 cellForRowAtIndexPath:indexPath];
    
    cell.startTime.text=_startstr;
    cell.upcomingDate.text=_datestr;
    cell.upcomingLocation.text=_locationstr;
    cell.upcomingSessionName.text=_sessionnamestr;
    cell.upcomingSessionDetails.text=_sessiondetailsstr;
    cell.sessionid.text=_tempSessid;
    cell.endtime.text=_endstr;
    
    UpcomingSession *u=self.upcomingSessions[indexPath.row];
    NSString *tempstr=u.sessionidStr;
    NSString *tempstr2=u.programidStr;
    _tempSessid=u.sessionidStr;
    _startstr=u.startTimeStr;
    _sessionnamestr=u.sessionNameStr;
    _sessiondetailsstr=u.sessionDetailsStr;
    _locationstr=u.roomnameStr;
    _endstr=u.endtimeStr;
    //NSLog(@"***temp str==%@ & temp str2==%@",tempstr,tempstr2);
    if([tempstr2 isEqualToString:@"4"])
    {
        [self performSegueWithIdentifier:@"showCourse" sender:nil];
    }
    if([tempstr2 isEqualToString:@"1"])
    {
        [self performSegueWithIdentifier:@"showSession" sender:self];
    }
    else if([tempstr2 isEqualToString:@"2"])
    {
        [self performSegueWithIdentifier:@"showKey" sender:nil];
    }
    else if([tempstr2 isEqualToString:@"3"])
    {
        [self performSegueWithIdentifier:@"showTalk" sender:nil];
    }
    else if([tempstr2 isEqualToString:@"5"])
    {
        [self performSegueWithIdentifier:@"showWomen" sender:self];
    }
    else if([tempstr2 isEqualToString:@"6"])
    {
        [self performSegueWithIdentifier:@"showPoster" sender:nil];
    }
    
    else if([tempstr2 isEqualToString:@"7"])
    {
        [self performSegueWithIdentifier:@"showBreakfast" sender:nil];
    }
    else if([tempstr2 isEqualToString:@"8"])
    {
        [self performSegueWithIdentifier:@"showInvited" sender:nil];
    }
    else if([tempstr2 isEqualToString:@"9"])
    {
        [self performSegueWithIdentifier:@"showPanel" sender:nil];
    }
    
    
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([[segue identifier]isEqualToString:@"showCourse"])
    {
        CourseViewController *s=[segue destinationViewController];
        s.temp_session_id=_tempSessid;
        s.stimestr=_startstr;
        s.locationstr=_locationstr;
        s.datestr=_datestr;
        s.sessionnamestr=_sessionnamestr;
        s.etimestr=_endstr;
    }
    if([[segue identifier]isEqualToString:@"showTalk"])
    {
        SessTalkViewController *stlk=[segue destinationViewController];
        stlk.session_idstr=_tempSessid;
        stlk.stime=_startstr;
        stlk.locationstr=_locationstr;
        stlk.etime=_endstr;
        stlk.sessNamestr=_sessionnamestr;
        //NSLog(@"IN DID SELECT after login...%@.....%@",_locationstr,_sessionnamestr);
        
    }
    
    if([[segue identifier]isEqualToString:@"showSession"])
    {
        SessionViewController *s=[segue destinationViewController];
        s.temp_session_id=_tempSessid;
        s.stimestr=_startstr;
        s.locationstr=_locationstr;
        s.datestr=_datestr;
        s.sessionstr=_sessionnamestr;
        s.etimestr=_endstr;
    }
    if([[segue identifier]isEqualToString:@"showWomen"])
    {
        WomenViewController *s=[segue destinationViewController];
        s.sessionidStr=_tempSessid;
        s.stimeStr=_startstr;
        s.locStr=_locationstr;
        s.etimeStr=_endstr;
    }
    if([[segue identifier]isEqualToString:@"showPanel"])
    {
        TechTalkNewViewController *t=[segue destinationViewController];
        t.tempsessionid=_tempSessid;
        t.stimeStr=_startstr;
        t.locStr=_locationstr;
        t.etimeStr=_endstr;
        
    }
    
    if([[segue identifier]isEqualToString:@"showPoster"])
    {
        PosterViewController *ps=[segue destinationViewController];
        ps.temp_seesionid_str=_tempSessid;
    }
    
    if([[segue identifier]isEqualToString:@"showBreakfast"])
    {
        BreakViewController *b=[segue destinationViewController];
        b.startstr=_startstr;
        b.endstr=_endstr;
        b.locationstr=_locationstr;
        
    }
    
    if([[segue identifier]isEqualToString:@"showKey"])
    {
        KeyViewController *k=[segue destinationViewController];
        k.sessionidstr=_tempSessid;
        k.starttimestr=_startstr;
        k.locationstr=_locationstr;
        k.keynotetiltestr=_sessionnamestr;
        k.endtimestr=_endstr;
        
    }
    
    if([[segue identifier]isEqualToString:@"showInvited"])
    {
        InvitedInfoViewController *i=[segue destinationViewController];
        i.session_idstr=_tempSessid;
        i.etime=_endstr;
        i.stime=_startstr;
        i.sessNamestr=_sessionnamestr;
        i.locationstr=_locationstr;
        
    }
    
    
}

-(void)buttonclicked:(id)sender
{
    
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    if (emailid==NULL) {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"Alert!" message:@"Please login to add in My Schedule"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        // NSLog(@"you pressed ok, please button");
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        
        [self mySchButtonclicked];
        
        UIButton *btn=sender;
        UpcomingSession *u=self.upcomingSessions[btn.tag];
        [btn setTitle:@"Added to my schedule" forState:UIControlStateNormal];        
        [self.upcomingSessions addObject:u];

        
    }
}

-(void)mySchButtonclicked{
    
    NSURLSessionConfiguration *config=[NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session=[NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSString *savedValue=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    
    NSString *sessionid =[[NSUserDefaults standardUserDefaults] stringForKey:@"sesionid"];
    NSString *programtypeid=[[NSUserDefaults standardUserDefaults] stringForKey:@"programtypeid"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@&session_id=%@&program_type_id=%@",savedValue,sessionid,programtypeid];
    //NSLog(@"mystttt==%@",myst);
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:schedule_session]];
    
    NSMutableURLRequest *urlrequest=[NSMutableURLRequest requestWithURL:url];
    [urlrequest setHTTPMethod:@"POST"];
    [urlrequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
        //NSLog(@"%@",error.description);
    }
    
    NSURLSessionDataTask *task=[session dataTaskWithRequest:urlrequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                {
                                    if (error) {
                                        NSLog(@"%@",error.description);
                                    }
                                    //NSString *text=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                                    //NSLog(@"data.....******=%@",text);
                                    NSError *error1=nil;
                                    NSDictionary *responsedict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
                                    if(error1)
                                    {
                                        //NSLog(@"error 1 is %@",error1.description);
                                    }
                                    NSLog(@"******json = %@",responsedict);
                                    [self getScheduledStatus];
                                }];
    [task resume];
    
}

-(void)getScheduledStatus{
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *sessionid =[[NSUserDefaults standardUserDefaults] stringForKey:@"sesionid"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@&session_id=%@",savedValue,sessionid];
    //[urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_schedule_status]];
    
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error == nil)
            {
                //NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                NSError *er=nil;
                NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                if(er)
                {
                    
                }
                
                //CHECK IF ALREADY ADDED INTO SESSION
                if(_upcomingSessions.count != 0){
                    UpcomingSession *k1=[_upcomingSessions objectAtIndex:0];
                    NSString *str1=[responseDict objectForKey:@"status"];
                    k1.status=str1;
                    [_upcomingSessions removeAllObjects];
                    [_upcomingSessions addObject:k1];
                    
                }
            }
        });
        [_tableview2 reloadData];
    }];
    [dataTask resume];
}

@end

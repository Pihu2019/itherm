//
//  AuthordetailTableViewCell.m
//  Whova1
//
//  Created by Anveshak on 11/10/16.
//  Copyright © 2016 Anveshak . All rights reserved.
//

#import "AuthordetailTableViewCell.h"

@implementation AuthordetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.image.layer.cornerRadius = self.image.frame.size.height /2;
    self.image.layer.masksToBounds = YES;
    self.image.layer.borderWidth = 1.0;
    self.image.layer.borderColor=(__bridge CGColorRef _Nullable)([UIColor lightGrayColor]);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

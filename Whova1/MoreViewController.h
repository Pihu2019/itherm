//
//  MoreViewController.h
//  ITherm
//
//  Created by Anveshak on 3/29/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *fullname;
@property (strong, nonatomic) IBOutlet UILabel *affiliation;
@property (strong,nonatomic) NSMutableArray *myprofile;
@property (strong,nonatomic) NSString *email,*userid;
@property (weak, nonatomic) IBOutlet UIView *viewToHide;
@property (weak, nonatomic) IBOutlet UIButton *votingButton;
@property (weak, nonatomic) IBOutlet UIImageView *votingButtonImg;
@property (weak, nonatomic) IBOutlet UILabel *votingButtonLabel;
@property (strong,nonatomic) NSString *paper,*paperidstr,*success,*message,*sessionidStr,*liked,*added;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@end


//
//  CollectionViewController.m
//  ITherm
//
//  Created by Sandeep Tonapi on 4/26/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import "CollectionViewController.h"
#import "CollectionViewCell.h"
#import "Exhibitor.h"
#import "Exhibition.h"
#import "Constant.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CollectionViewController (){
    NSArray * devices;
    
}

@end

@implementation CollectionViewController
static NSString * const reuseIdentifier = @"Cell";
static NSInteger *flag;

- (void)viewDidLoad {
    [super viewDidLoad];
    _namearr=[[NSMutableArray alloc]init];
    _numberarr=[[NSMutableArray alloc]init];
    _imgarray=[[NSMutableArray alloc]init];
    _artidArr=[[NSMutableArray alloc]init];
    _likedArr=[[NSMutableArray alloc]init];
    _exhibitionArr=[[NSMutableArray alloc]init];
    _artArr=[[NSMutableArray alloc]init];
    _tickArr=[[NSMutableArray alloc]init];
    
    //DISCLAIMER
    
    NSString *str1 = @"Hello all the attendees, Grand Welcome for the Art in Science Competition.";
    NSString *str2 = @"Following are the rules for voting";
    NSString *str3 = @"1. Any attendee can only vote for at most 3 entries.";
    NSString *str4 = @"2. Once voted for any entry, the action can not be reversed, so vote wisely and attentively.";
    NSString *disclaimer = [NSString stringWithFormat:@"%@\n%@\n%@\n%@", str1, str2, str3,str4];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"DISCLAIMER" message:disclaimer delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alert show];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self parsingArtExhibition];
    [self getVoteCount:NULL];
}


- (IBAction)voteTapped:(UIButton *)sender {
    [sender setTitle:@"Voted" forState:UIControlStateDisabled];
    sender.enabled = NO;
    Exhibition *temp = [_exhibitionArr objectAtIndex:sender.tag];
    [_exhibitionArr removeObjectAtIndex:sender.tag];
    temp.voted_status = @"1";
    [_exhibitionArr insertObject:temp atIndex:sender.tag];
    [self Vote];
    [self getVoteCount:sender];
}

-(void)Vote
{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Vote is being submitted..." message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator startAnimating];
    
    [alertView setValue:indicator forKey:@"accessoryView"];
    [alertView show];
    [self performSelector:@selector(dismissAlert:) withObject:alertView afterDelay:5.0f];
    
    
    
    NSString *savedValue=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    //NSString *contestantId=[[NSUserDefaults standardUserDefaults]stringForKey:@"_contestant_id"];
    NSString *myst=[NSString stringWithFormat:@"emailid=%@&contestant_id=%@",savedValue,self.contestant_id];
    
    NSURLSessionConfiguration *config=[NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session=[NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:vote_for_image]];
    
    NSMutableURLRequest *urlrequest=[NSMutableURLRequest requestWithURL:url];
    
    [urlrequest setHTTPMethod:@"POST"];
    [urlrequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
        NSLog(@"%@",error.description);
    }
    
    NSURLSessionDataTask *task=[session dataTaskWithRequest:urlrequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                {
                                    if (error)
                                    {
                                        NSLog(@"%@",error.description);
                                    }
                                    NSString *text=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                                    //NSLog(@"Response of voting =%@",text);
                                    
                                    NSError *error1=nil;
                                    NSDictionary *responsedict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
                                    if(error1)
                                    {
                                        NSLog(@"error 1 is %@",error1.description);
                                    }
                                    NSLog(@"Response from dictonary:%@",responsedict);
                                    
                                    self.success=[responsedict objectForKey:@"success"];
                                    
                                    //NSLog(@"Value of success:%@",_success);
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        if([self.success isEqualToString:@"1"])
                                        {
                                            //NSLog(@"Voted Successfully");
                                            [self viewWillAppear:YES];
                                            [self.collectionView reloadData];
                                            
                                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"                                                                                            message:@"Voted successfully."delegate:self
                                                                                  cancelButtonTitle:@"OK"otherButtonTitles:nil];
                                            [alert show];
                                            
                                        }
                                        else
                                        {
                                            //NSLog(@"Voting Failure");
                                            //voted and disable
                                        }
                                    });
                                    
                                    NSLog(@"json = %@",responsedict);
                                    
                                    
                                }];
    [task resume];
    
}

/*-(void)indicatorLoader:(UITapGestureRecognizer *)sender{
 CGPoint location = [sender locationInView:sender.view];
 NSIndexPath *indexPath = [(UICollectionView *)sender.view.superview.superview.superview indexPathForItemAtPoint:location];
 CollectionViewCell *cell = [(UICollectionView *)sender.view.superview.superview.superview cellForItemAtIndexPath:indexPath];
 [cell.indicator startAnimating];
 
 }*/
-(void)dismissAlert:(UIAlertView *) alertView
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)getVoteCount:(UIButton *)sender{
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];
    
    //NSLog(@"My st:%@",myst);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_votes_by_user]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error == nil)
            {
                //NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
            }
            if(data != nil){
                NSError *er=nil;
                NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                //NSLog(@"Response Dict :%@",responseDict);
                
                _vote_count = [responseDict objectForKey:@"votes"];
                NSUserDefaults *voteCount = [NSUserDefaults standardUserDefaults];
                [voteCount setObject:_vote_count forKey:@"voteCount"];
                [voteCount synchronize];
                if(sender!=NULL){
                    //                    CollectionViewCell *c = (CollectionViewCell *)sender.superview.superview;
                    //                    [c setUserInteractionEnabled:NO];
                    //                    [c setAlpha:0.5];
                }
                NSLog(@"No of votes : %@",_vote_count);
                if(er)
                {
                }
            }
            
        });
    }];
    [dataTask resume];
}

//-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
//    CollectionViewCell *cell;
//    return  cell.kimagev.image;
//}

-(void)parsingArtExhibition
{
    queue=dispatch_queue_create("images", DISPATCH_QUEUE_CONCURRENT);
    queue=dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0);
    
    [_namearr removeAllObjects];
    [_imgarray removeAllObjects];
    [_numberarr removeAllObjects];
    [_exhibitionArr removeAllObjects];
    [_artidArr removeAllObjects];
    
    _newurlpass=_urlstr;
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];
    
    NSURLSessionConfiguration *configuration=[NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration: configuration delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:get_art_sci_data]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURLSessionDataTask *task=[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                {
                                    
                                    if(data==nil)
                                    {
                                        NSLog(@"Data is nil");
                                        
                                    }
                                    
                                    else
                                    {
                                        
                                        NSDictionary *outerdic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                        
                                        NSArray *keyarr=[outerdic objectForKey:@"art_sci_data"];
                                        
                                        //NSLog(@"Key array : *****%@",keyarr);
                                        
                                        for(NSDictionary *temp in keyarr)
                                        {
                                            NSString *str1 = [[temp objectForKey:@"contestant_name"]description];
                                            NSString *str2 = [[temp objectForKey:@"id"]description];
                                            NSString *str3 = [temp objectForKey:@"images"];
                                            NSString *str4 = [temp objectForKey:@"contestant_id"];
                                            NSString *str5 = [temp objectForKey:@"entry_title"];
                                            NSString *str6 = [temp objectForKey:@"entry_description"];
                                            NSString *str7 = [temp objectForKey:@"voted_status"];
                                            //NSString *str8 = [temp objectForKey:@"contestant_id"];
                                            
                                            [_imgarray addObject:str3];
                                            //NSLog(@"str4  %@",str3);
                                            
                                            Exhibition *k1=[[Exhibition alloc]init];
                                            k1.name=str1;
                                            k1.number=str2;
                                            k1.imgurl=str3;
                                            k1.artidStr=str4;
                                            k1.imgTitle=str5;
                                            k1.imgDescription=str6;
                                            k1.voted_status=str7;
                                            //k1.contenstant_id=str4;
                                            
                                            [[NSUserDefaults standardUserDefaults]setValue:str4 forKey:@"art_id"];
                                            
                                            [_exhibitionArr addObject:k1];
                                            
                                            [self.collectionView reloadData];
                                        }
                                        [self.collectionView reloadData];
                                    }
                                    [self.collectionView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
                                }];
    [task resume];
    
    [self.collectionView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _exhibitionArr.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    //zooming
    [cell setup];
    
    [self getVoteCount:NULL];
    
    cell.contentView.clipsToBounds = true;
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    
    Exhibition *ktemp=[_exhibitionArr objectAtIndex:indexPath.row];
    
    NSLog(@"Exhibition :%@",ktemp);
    if(ktemp.name != NULL){
        cell.name.text=ktemp.name;
    }
    if(ktemp.artidStr != NULL){
        cell.number.text=ktemp.artidStr;
        self.contestant_id = ktemp.artidStr;
    }
    
    if(ktemp.imgTitle != NULL){
        cell.imgTitle.text=ktemp.imgTitle;
    }
    
    if(ktemp.imgDescription != NULL){
        cell.imgDescription.text=ktemp.imgDescription;
    }
    
    
    /*UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(indicatorLoader:)];
     gesture.numberOfTapsRequired = 1;
     [cell.votingButton addGestureRecognizer:gesture];
     */
    
    
    
    //BUTTON TAP
    cell.votingButton.tag = indexPath.row;
    [cell.votingButton addTarget:self action:@selector(voteTapped:) forControlEvents:UIControlEventTouchUpInside];
    //check no of votes and set the button status
    NSUserDefaults *voteCount = [NSUserDefaults standardUserDefaults];
    NSString *temp = [voteCount objectForKey:@"voteCount"];
    NSInteger b = [temp integerValue];
    [voteCount synchronize];
    NSLog(@"Value of count: %ld",(long)b);
    
    //TO CHECK VOTED STATUS
    //NSLog(@"Check voted status");
    if([ktemp.voted_status isEqualToString:@"1"]){
        [cell.votingButton setTitle: @"Voted" forState: UIControlStateDisabled];
        cell.votingButton.enabled = NO;
    }else if(b >= 3){
        //NSLog(@"Greater");
        [cell.votingButton setTitle: @"Maximum limit reached" forState: UIControlStateDisabled];
        cell.votingButton.enabled = NO;
    }else{
        //NSLog(@"else inside this");
        [cell.votingButton setTitle: @"Vote" forState: UIControlStateNormal];
        cell.votingButton.enabled = YES;
    }
    
    
    //zooming of image
    //    cell.scrollViewImage.autoresizesSubviews = YES;
    //    cell.scrollViewImage.multipleTouchEnabled = YES;
    //    cell.scrollViewImage.maximumZoomScale = 4.0;
    //    cell.scrollViewImage.minimumZoomScale = 1.0;
    //    cell.scrollViewImage.clipsToBounds = YES;
    //    cell.scrollViewImage.delegate = self;
    //    cell.scrollViewImage.zoomScale = 1.0;
    //
    
    //TO SET IMAGE
    static UIImage *placeholderImage = nil;
    if (!placeholderImage) {
        placeholderImage = [UIImage imageNamed:@"placeholder"];
    }
    
    NSString *kImgLink=[_imgarray objectAtIndex:indexPath.row];
    
    NSString *str = [NSString stringWithFormat: @"%@%@",imagesUrl,kImgLink];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.kimagev sd_setImageWithURL:[NSURL URLWithString:str]];
            //cell.kimagev.image = img;
        });
    });
    
    return cell;
}


- (void)flushCache
{
    [SDWebImageManager.sharedManager.imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearDiskOnCompletion:nil];
}


//https://stackoverflow.com/questions/31662155/how-to-change-uicollectionviewcell-size-programmatically-in-swift
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame), (CGRectGetHeight(collectionView.frame)));
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

#pragma mark <UICollectionViewDelegate>

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
 return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
 return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
 
 }
 */

@end

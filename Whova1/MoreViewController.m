//
//  MoreViewController.m
//  ITherm
//
//  Created by Anveshak on 3/29/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import "MoreViewController.h"
#import "Attendee.h"
#import "Constant.h"

@interface MoreViewController (){
    NSString *startdate,*enddate,*currenttime;
    NSDateFormatter *dateFormatter;
    NSDate *startDate,*endDate,*currentTime;
    
}

@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _image.layer.masksToBounds=YES;
    _image.layer.borderWidth=1.1;
    _image.layer.cornerRadius=_image.frame.size.width/2;
    
    _image.layer.borderColor=([UIColor colorWithRed:(211.0/225.0) green:(211.0/225.0) blue:(211.0/255.0)alpha:1.0].CGColor);
    
    _image.clipsToBounds=YES;
    
    //DATE CONDITIONS
    startdate = @"29-05-2018 07:00:00";
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    startDate = [dateFormatter dateFromString:startdate];
    NSLog(@"Start Date :%@",startDate);
    
    enddate = @"31-05-2018 21:00:00";
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    endDate = [dateFormatter dateFromString:enddate];
    NSLog(@"End Date :%@",endDate);
    
    
    
    //Get server time
    [ self getServerDate];
    
    NSDate *dateTemp = [NSDate date];
    
    //get current date of system
    //currenttime = @"29-05-2018 07:45:44";
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    currenttime = [dateFormatter stringFromDate:dateTemp];
    currentTime = [dateFormatter dateFromString:currenttime];
    NSLog(@"Current Date %@", currentTime);
    //[outputFormatter release];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *role_id = [defaults objectForKey:@"role_id"];
    NSLog(@"Role Id of Logged in user :%@",role_id);
    
    if([role_id  isEqual: @"13"]){
        NSLog(@"Admin user");
        _votingButton.hidden = NO;
        _votingButtonImg.hidden = NO;
        _votingButtonLabel.hidden = NO;
        
    }else{
        NSLog(@"Normal user");
        _votingButton.hidden = YES;
        _votingButtonImg.hidden = YES;
        _votingButtonLabel.hidden = YES;
    }
    
}

-(void)getServerDate{
    
    // making a GET request to /init
    NSString *targetUrl = [NSString stringWithFormat:@"%@", [mainUrl stringByAppendingString:get_date]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
          NSLog(@"Data received: %@", myString);
          //currenttime = myString;
          
      }] resume];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    if (emailid==NULL) {
        self.fullname.text=@"Guest";
        _viewToHide.hidden = YES;
    }
    else {
        
        [self getNameParsing];
        _viewToHide.hidden = NO;
        
    }
    
}



- (IBAction)artinsciencetapped:(UIButton *)sender {
    //check if user is logged in ?
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
    NSLog(@"Emaiil id :%@",emailid);
    if([emailid length] == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                        message:@"Please login first."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }else{
        
        //user is logged in so check if voting started
        if([startDate compare:currentTime] == NSOrderedAscending) {
            // dateOne is before dateTwo
            if([currentTime compare:endDate] == NSOrderedAscending){
                //check here if the attendee is valid
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString *valid = [defaults stringForKey:@"valid_status"];
                NSLog(@"Valid :***%@",valid);
                if([valid isEqualToString: @"1"]){
                    //user is both valid and logged in
                    NSLog(@"Start Voting");
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"votingScreen"];
                    [self.navigationController pushViewController:vc animated:YES];
                
                }else{
                    //no allowed
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                                    message:@"Only registered attendee can vote for the competition"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
                
            }else{
                NSLog(@"Voting ended");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                                message:@"Voting ended."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }else{
            NSLog(@"Voting yet to start");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                            message:@"Voting period is 29th May 07:00 AM to 31st May 09:00 PM"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}


- (IBAction)votingReportTapped:(UIButton *)sender {
    [_indicator startAnimating];
    
    //check for timings then segue
    if([startDate compare:currentTime] == NSOrderedAscending) {
        // dateOne is before dateTwo
        if([currentTime compare:endDate] == NSOrderedAscending){
            
            [_indicator stopAnimating];
            NSLog(@"Voting period isn't over yet.");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                            message:@"Voting period isn't over yet."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            
            
        }else{
            NSLog(@"Voting ended");
            
            //REQUEST NETWORK Generate report
            NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"email"];
            
            NSArray *role_id = [[NSUserDefaults standardUserDefaults]objectForKey:@"role_id"];
            
            NSString *myst=[NSString stringWithFormat:@"emailid=%@&role=%@",emailid,role_id];
            
            NSLog(@"My string is :%@",myst);
            
            NSURLSessionConfiguration *config=[NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *session=[NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
            
            NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:send_report_local]];
            
            NSMutableURLRequest *urlrequest=[NSMutableURLRequest requestWithURL:url];
            [urlrequest setHTTPMethod:@"POST"];
            [urlrequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
            NSError *error=nil;
            if(error)
            {
                NSLog(@"%@",error.description);
            }
            
            NSURLSessionDataTask *task=[session dataTaskWithRequest:urlrequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                        {
                                            
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                if (error)
                                                {
                                                    NSLog(@"%@",error.description);
                                                }
                                                //NSString *text=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                                                
                                                NSError *error1=nil;
                                                
                                                NSDictionary *responsedict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
                                                if(error1)
                                                {
                                                    NSLog(@"error 1 is %@",error1.description);
                                                }
                                                
                                                NSLog(@"Repsonse: = %@",responsedict);
                                                
                                                NSString *temp = responsedict[@"status"];
                                                
                                                if([temp isEqualToString:@"1"]){
                                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                                                                    message:@"Report sent to registered email."
                                                                                                   delegate:self
                                                                                          cancelButtonTitle:@"OK"
                                                                                          otherButtonTitles:nil];
                                                    [alert show];
                                                }else{
                                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                                                                    message:@"Problem sending report."
                                                                                                   delegate:self
                                                                                          cancelButtonTitle:@"OK"
                                                                                          otherButtonTitles:nil];
                                                    [alert show];
                                                    
                                                }
                                                [_indicator stopAnimating];
                                            });
                                            
                                        }];
            [task resume];
            
        }
    }else{
        NSLog(@"Voting yet to start");
        [_indicator stopAnimating];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                        message:@"Voting period is 29th May 07:00 AM to 31st May 09:00 PM"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}




-(void)getNameParsing{
    
    Attendee *a1=[[Attendee alloc]init];
    
    self.myprofile=[[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    
    NSString *myst=[NSString stringWithFormat:@"emailid=%@",savedValue];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[mainUrl stringByAppendingString:getprofile]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    
    [urlRequest setHTTPBody:[myst dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error=nil;
    
    if(error)
    {
        NSLog(@"Error encountered");
    }
    
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(data != nil){
                                              if(error)
                                              {
                                                  NSLog(@"Error encountered");
                                              }
                                              
                                              //NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                              NSError *er=nil;
                                              NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                              if(er)
                                              {
                                              }
                                              
                                              NSArray *myattend=[responseDict objectForKey:@"attendees"];
                                              
                                              for(NSDictionary * dict in myattend)
                                              {
                                                  
                                                  a1.first_name=dict[@"first_name"];
                                                  a1.last_name=[dict objectForKey:@"last_name"];
                                                  a1.salutation=[dict objectForKey:@"salutation"];
                                                  a1.mobile_no=[dict objectForKey:@"mobile_no"];
                                                  a1.country_name=[dict objectForKey:@"country_name"];
                                                  a1.role_name=[dict objectForKey:@"role_name"];
                                                  a1.emailid=[dict objectForKey:@"emailid"];
                                                  a1.role_name=[dict objectForKey:@"role_name"];
                                                  a1.affiliation=[dict objectForKey:@"affiliation"];
                                                  a1.fax=dict[@"fax"];
                                                  //   a1.mainid=dict[@"id"];
                                                  a1.userid=dict[@"user_id"];
                                                  self.userid=dict[@"user_id"];
                                                  // self.myid=dict[@"id"];
                                                  
                                                  [self.myprofile addObject:a1];
                                                  
                                                  
                                                  //FETCH AND SET IMAGE
                                                  NSString * profile_picUrl = [dict objectForKey:@"profile_pic"];
                                                  NSURL * url = [NSString stringWithFormat:@"%@%@", imagesUrl,profile_picUrl];
                                                  
                                                  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                      // retrive image on global queue
                                                      UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:url]]];
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          // assign cell image on main thread
                                                          _image.image = img;
                                                      });
                                                  });
                                                  
                                              }
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                                  if(a1.first_name==(NSString *) [NSNull null])
                                                  {
                                                      a1.first_name=@"not given";
                                                  }
                                                  if(a1.last_name==(NSString *) [NSNull null])
                                                  {
                                                      a1.last_name=@"not given";
                                                  }
                                                  if(a1.salutation==(NSString *) [NSNull null])
                                                  {
                                                      a1.salutation=@"";
                                                  }
                                                  if(a1.role_name==(NSString *) [NSNull null])
                                                  {
                                                      a1.role_name=@"not given";
                                                  }
                                                  if(a1.country_name==(NSString *) [NSNull null])
                                                  {
                                                      a1.country_name=@"not given";
                                                  }
                                                  if(a1.mobile_no==(NSString *) [NSNull null])
                                                  {
                                                      a1.mobile_no=@"";
                                                  }
                                                  if(a1.fax==(NSString *) [NSNull null])
                                                  {
                                                      a1.fax=@"";
                                                  }
                                                  if(a1.affiliation==(NSString *) [NSNull null])
                                                  {
                                                      a1.affiliation=@"---";
                                                  }
                                                  
                                                  self.affiliation.text=a1.affiliation;
                                                  
                                                  self.fullname.text = a1.emailid;
                                                  
                                                  //NSLog(@"fullname==%@",self.fullname.text);
                                                  
                                              });
                                          }
                                      }];
    
    [dataTask resume];
    
}

- (IBAction)logoutClicked:(UIButton *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"email"];
    
    //LOGOUT
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"identifier"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"allnotification"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"role_id"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"voteCount"];
    
}



@end

//
//  PaperDetails.h
//  ITherm
//
//  Created by Anveshak on 11/22/17.
//  Copyright © 2017 Anveshak . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaperDetails : NSObject
@property (strong,nonatomic) NSString *start_time,*date,*paper_name,*room_name;
@end

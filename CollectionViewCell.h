//
//  CollectionViewCell.h
//  ITherm
//
//  Created by Sandeep Tonapi on 4/26/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *kimagev;
@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UILabel *count_like;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewImage;

@property (weak, nonatomic) IBOutlet UILabel *imgTitle;

@property (weak, nonatomic) IBOutlet UILabel *imgDescription;
@property (weak, nonatomic) IBOutlet UIButton *votingButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (void)setup;


@end

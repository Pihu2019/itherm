//
//  CollectionViewCell.m
//  ITherm
//
//  Created by Sandeep Tonapi on 4/26/18.
//  Copyright © 2018 Anveshak . All rights reserved.
//

#import "CollectionViewCell.h"
#define MAXIMUM_SCALE 3.0
#define MINIMUM_SCALE 1.0

@interface CollectionViewCell()<UIScrollViewDelegate>
@end

@implementation CollectionViewCell
- (void)setup {
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoomImage:)];
    self.kimagev.gestureRecognizers = @[pinch];
    self.kimagev.userInteractionEnabled = YES;
    self.scrollViewImage.delegate = self;
    _votingButton.layer.cornerRadius = 10;
    
}

#pragma  mark  - Scrollview Delegate Method

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.kimagev;
}

//@implementation cutomCollectionViewCell



//-----------------------------------------------------------------------

#pragma mark - Custom Methods

- (void)zoomImage:(UIPinchGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded
        || gesture.state == UIGestureRecognizerStateChanged) {
        NSLog(@"gesture.scale = %f", gesture.scale);
        
        CGFloat currentScale = self.frame.size.width / self.bounds.size.width;
        CGFloat newScale = currentScale * gesture.scale;
        
        if (newScale < MINIMUM_SCALE) {
            newScale = MINIMUM_SCALE;
        }
        if (newScale > MAXIMUM_SCALE) {
            newScale = MAXIMUM_SCALE;
        }
        
        CGAffineTransform transform = CGAffineTransformMakeScale(newScale, newScale);
        self.kimagev.transform = transform;
        self.scrollViewImage.contentSize = self.kimagev.frame.size;
        
    }
    
}

@end
